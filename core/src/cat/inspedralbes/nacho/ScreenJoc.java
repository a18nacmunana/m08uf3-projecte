package cat.inspedralbes.nacho;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class ScreenJoc implements Screen{
	Joc joc;
	Stage stage;
	Texture texPlayer;
	Texture texObstacle1;
	Texture texObstacle2;
	Marcador marcador;
	Player player;
	List <Obstacle> obstacles;
	float obstacleVelocity= Constants.INITIAL_OBSTACLES_VELOCITY;
	float secondsObstacle=0;
	float obstacleTimer=1.5f;
	float secondsDifficulty=0;
	float difficultyTimer=5;
	float secondsVelocity=0;
	float velocityTimer=3;
	Random rand = new Random();
	
	public ScreenJoc(Joc joc) {
		this.joc = joc;
		texObstacle1 = new Texture("barrel.png");
		texObstacle2 = new Texture("player_blue.png");
		player= new Player();
		obstacles = new ArrayList<>();
		marcador= new Marcador();
		
		stage = new Stage(new ScreenViewport());
		ArrayList<Texture> textures = new ArrayList<Texture>();
        for(int i = 1; i <=6;i++){
            textures.add(new Texture("parallax/img"+i+".png"));
            textures.get(textures.size()-1).setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
        }

        ParallaxBackground parallaxBackground = new ParallaxBackground(textures);
        parallaxBackground.setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        parallaxBackground.setSpeed(1);
        stage.addActor(parallaxBackground);
		
	}
	
	@Override
	public void render(float delta) {
		secondsVelocity+=delta;
		secondsDifficulty+=delta;
		secondsObstacle+=delta;
		
		controlPlayer();	
		controlObstacles();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
        stage.draw();
		joc.batch.begin();
		player.draw(joc.batch, delta);
		for(Obstacle obstacle : obstacles)
			obstacle.draw(joc.batch);
		marcador.draw(joc.batch);
		joc.batch.end();
	}
	
	private void controlPlayer() {
		if(Gdx.input.isKeyJustPressed(Input.Keys.UP) && !player.isJumping && !player.isCrouched){
			player.isJumping=true;
		}
		else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			if(player.isJumping==true) {
				if(!player.reachedTop) {
					player.reachedTop=true;
					player.jumpVel=0;
				}
					player.jumpVel-=50;
			}
			else {
				player.isCrouched=true;
			}
		}
		else {
			player.isCrouched=false;
		}
	}
	
	private void controlObstacles() {
		//Redueix el temps de spawn
		if(secondsDifficulty>=difficultyTimer) {
			if(obstacleTimer>0.75) {
				obstacleTimer-=0.25;
				System.out.println("Obs spawn = "+obstacleTimer);
			}
			secondsDifficulty=0;
		}
		
		//Augmenta la velocitat dels obstacles
		if(secondsVelocity>=velocityTimer) {
			if(obstacleVelocity<18){
				obstacleVelocity+=1;
				System.out.println("Obs vel = "+obstacleVelocity);
			}
			secondsVelocity=0;
		}
		
		
		for(Obstacle obstacle : obstacles) {
			obstacle.move();
		}
		
		for (Iterator<Obstacle> iterObs = obstacles.iterator(); iterObs.hasNext();) {
			Obstacle obstacle = iterObs.next();
			if(obstacle.rec.x-obstacle.rec.width<=Constants.PLAYER_X && !obstacle.dodged) {
				marcador.increment();
				obstacle.dodged=true;
			}
			if (obstacle.rec.x < -obstacle.rec.width) {
				iterObs.remove();
			}
			if(player.overlaps(obstacle.rec)) {
				System.out.println("Has xocat");
				Gdx.app.exit();
			}
		}
		
		if(secondsObstacle>=obstacleTimer) {
			int obstacleType = rand.nextInt((2-1)+1)+1;
			Obstacle obstacle;

			if(obstacleType==1) {
				obstacle = new Obstacle(texObstacle1, Constants.WIDTH_WORLD, Constants.OBSTACLE_1_Y, Constants.OBSTACLE_1_WIDTH, Constants.OBSTACLE_1_HEIGHT, obstacleVelocity);
			}
			else {
				obstacle = new Obstacle(texObstacle2, Constants.WIDTH_WORLD, Constants.OBSTACLE_2_Y, Constants.OBSTACLE_2_WIDTH, Constants.OBSTACLE_2_HEIGHT, obstacleVelocity);

			}
			obstacles.add(obstacle);
			secondsObstacle = 0;
		}
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		
	}
	
}
