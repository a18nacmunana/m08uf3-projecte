package cat.inspedralbes.nacho;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Marcador {
	ArrayList<Texture> texNums = new ArrayList<>();
	int num1=0;
	Rectangle rec1;
	int num2=0;
	Rectangle rec2;
	int num3=0;
	Rectangle rec3;
	
	
	Marcador(){
		for(int i=0;i<=9;i++) {
			texNums.add(new Texture("numbers/num"+i+".jpg"));
		}
		rec1 = new Rectangle(10,Constants.HEIGHT_WORLD-Constants.MARCADOR_HEIGHT, Constants.MARCADOR_WIDTH, Constants.MARCADOR_HEIGHT);
		rec2 = new Rectangle(10+Constants.MARCADOR_WIDTH,Constants.HEIGHT_WORLD-Constants.MARCADOR_HEIGHT, Constants.MARCADOR_WIDTH, Constants.MARCADOR_HEIGHT);
		rec3 = new Rectangle(10+Constants.MARCADOR_WIDTH*2,Constants.HEIGHT_WORLD-Constants.MARCADOR_HEIGHT, Constants.MARCADOR_WIDTH, Constants.MARCADOR_HEIGHT);

	}
	
	public void increment() {
		if(num3<9)
			num3++;
		else {
			if(num2<9) {
				num2++;
				num3=0;
			}else {
				num1++;
				num2=0;
				num3=0;
			}
				
		}
	}
	
	public void draw(SpriteBatch openBatch) {
		openBatch.draw(texNums.get(num1), rec1.x, rec1.y, rec1.width, rec1.height);
		openBatch.draw(texNums.get(num2), rec2.x, rec2.y, rec2.width, rec2.height);
		openBatch.draw(texNums.get(num3), rec3.x, rec3.y, rec3.width, rec3.height);
	}
}
