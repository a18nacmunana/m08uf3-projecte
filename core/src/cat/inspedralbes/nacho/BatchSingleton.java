package cat.inspedralbes.nacho;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BatchSingleton {
	
	public SpriteBatch batch;
	
	private static BatchSingleton instance = new BatchSingleton();
	
	private BatchSingleton() {
		batch = new SpriteBatch();
	}

	public static BatchSingleton getInstance() {
		return instance;
	}
}
