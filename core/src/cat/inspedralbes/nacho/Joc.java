package cat.inspedralbes.nacho;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Joc extends Game {
	
	public SpriteBatch batch;
	public Viewport viewport;
	public Camera camera;
	@Override
	public void create () {
		batch = BatchSingleton.getInstance().batch;
		camera = new OrthographicCamera();
		viewport = new FitViewport(Constants.WIDTH_WORLD, Constants.HEIGHT_WORLD, camera);
		
		camera.position.set(Constants.WIDTH_WORLD / 2f, Constants.HEIGHT_WORLD / 2f, 0);
		camera.update();
		
		setScreen(new ScreenInici(this));
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		viewport.update(width, height);
	}

	@Override
	public void render () {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		super.render();
	}
	
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
