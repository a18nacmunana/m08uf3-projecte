package cat.inspedralbes.nacho;

public class Constants {
	static final float GRAVITY=25.63f;
	static final float WIDTH_WORLD=800;
	static final float HEIGHT_WORLD=280;
	
	static final float PLAYER_X=60;
	static final float PLAYER_Y=25;
	static final float PLAYER_WIDTH=50;
	static final float PLAYER_HEIGHT=50;
	static final float PLAYER_CROUCHED_X=75;
	static final float PLAYER_CROUCHED_Y=25;
	static final float PLAYER_CROUCHED_WIDTH=30;
	static final float PLAYER_CROUCHED_HEIGHT=30;
	
	static final float INITIAL_OBSTACLES_VELOCITY=10;
	static final float OBSTACLE_1_Y=25;
	static final float OBSTACLE_1_WIDTH=25;
	static final float OBSTACLE_1_HEIGHT=40;
	static final float OBSTACLE_2_Y=60;
	static final float OBSTACLE_2_WIDTH=20;
	static final float OBSTACLE_2_HEIGHT=40;
	
	static final float INITIAL_JUMP_VELOCITY=440;
	static final float FLOOR_HEIGHT=25;

	static final float MARCADOR_WIDTH=50;
	static final float MARCADOR_HEIGHT=50;
}
