package cat.inspedralbes.nacho;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class ScreenInici implements Screen{
	Texture texLogo;
	private static final float TIME_SPLASH = 1;
	float splashSeconds = 0;
	final Joc joc;

	
	public ScreenInici(Joc joc) {
		texLogo = new Texture("logo_ins.jpg");
		this.joc = joc;
	}

	@Override
	public void render(float delta) {
		if (splashSeconds < TIME_SPLASH) {
			splashSeconds += delta;
			joc.batch.begin();
			//329x117
			joc.batch.draw(texLogo, 0, 0, Gdx.graphics.getWidth(), 284);
			joc.batch.end();
		}else {
			joc.setScreen(new ScreenJoc(joc));
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}

