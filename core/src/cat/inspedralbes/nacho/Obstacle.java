package cat.inspedralbes.nacho;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Obstacle {
	Texture tex;
	Rectangle rec;
	float velocity;
	boolean dodged;
	
	Obstacle(Texture tex,float x, float y, float ample, float alt, float vel){
		this.tex= tex;
		rec = new Rectangle(x, y, ample, alt);
		velocity = vel;
		dodged=false;
	}
	
	public void move() {
		rec.x -= velocity;
	}
	
	public void draw(SpriteBatch openBatch) {
		openBatch.draw(tex, rec.x, rec.y, rec.width, rec.height);
	}
	
	public boolean overlaps(Rectangle r) {
		return rec.overlaps(r);
	}
}
