package cat.inspedralbes.nacho;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Player {
	Texture tex;
	Texture texCrouched;
	Rectangle rec;
	Rectangle recCrouched;
	boolean isCrouched;
	boolean isJumping;
	boolean reachedTop;
	float jumpVel;
	
	public Player() {
		tex= new Texture("ninja.png");
		texCrouched=new Texture("ninja.png");
		rec= new Rectangle(Constants.PLAYER_X, Constants.PLAYER_Y ,Constants.PLAYER_WIDTH, Constants.PLAYER_HEIGHT);
		recCrouched= new Rectangle(Constants.PLAYER_CROUCHED_X, Constants.PLAYER_CROUCHED_Y, Constants.PLAYER_CROUCHED_WIDTH, Constants.PLAYER_CROUCHED_HEIGHT);
		isCrouched=false;
		isJumping=false;
		reachedTop=false;
		jumpVel=Constants.INITIAL_JUMP_VELOCITY;
	}
	
	public void move(float delta) {
		
		 /* pos_y = pos_y + (velocity_y * time_difference) + (gravity_y * (time_difference ^ 2) / 2)
		 * velocity_y = velocity_y + (acceleration_y * time_difference)*/
		
		if(isJumping) {
			if(jumpVel<=0)
				reachedTop=true;
			
			rec.y+=jumpVel*delta;
			jumpVel-=Constants.GRAVITY;
			
			if(rec.y<=Constants.FLOOR_HEIGHT) {
				rec.y=Constants.FLOOR_HEIGHT;
				isJumping=false;
				reachedTop=false;
				resetJumpVel();
			}
			
		}
		
	}
	
	private void resetJumpVel() {
		jumpVel=Constants.INITIAL_JUMP_VELOCITY;
	}
	
	public void draw(SpriteBatch openBatch, float delta) {
		move(delta);
		if(isCrouched) 
			openBatch.draw(texCrouched, recCrouched.x, recCrouched.y, recCrouched.width, recCrouched.height);
		else
			openBatch.draw(tex, rec.x, rec.y, rec.width, rec.height);
	}
	
	public boolean overlaps(Rectangle r) {
		if(isCrouched)
			return recCrouched.overlaps(r);
		return rec.overlaps(r);
	}
	
}
